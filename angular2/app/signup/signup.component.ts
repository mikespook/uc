import { Component } from '@angular/core';

@Component({
	template: `
<h2>Singup</h2>
<p><a [routerLink]="['/login']">Login</a></p>
<p><a [routerLink]="['/repass']">Retrieve Password</a></p>
<form role="form" (submit)="signup($event, email.value, password.value, repass.value, name.value, address.value, city.value, state.value, country.value, tel.value)">
	<input #email id="email" type="text" Placeholder="Email">
	<input #password id="password" type="password" Placeholder="Password">
	<input #repass id="repass" type="password" Placeholder="Repeat Password">
	
	<input #name id="name" type="text" Placeholder="Name">
	<input #address id="address" type="text" Placeholder="Address">
	<input #city id="city" type="text" Placeholder="City">
	<input #state id="state" type="text" Placeholder="State">
	<input #country id="country" type="text" Placeholder="Country">
	<input #tel id="tel" type="text" Placeholder="Tel">
	<button type="submit">Login</button>
</form>`
})

// Component class
export class SignupComponent {}
