import { Component } from '@angular/core';
import {ROUTER_DIRECTIVES, Router, Routes} from "@angular/router";
import { Http, Response, HTTP_PROVIDERS } from '@angular/http';

@Component({
	template: `
<h2>Login</h2>
<p><a [routerLink]="['/signup']">Signup</a></p>
<p><a [routerLink]="['/repass']">Retrieve Password</a></p>
<form role="form" (submit)="login($event, email.value, password.value)">
	<input #email name="email" id="email" type="text" Placeholder="Email">
	<input #password name="password" id="password" type="password" Placeholder="Password">
	<button type="submit">Login</button>
</form>`,
	providers: [HTTP_PROVIDERS],
	directives: [ROUTER_DIRECTIVES]
})

// Component class
export class LoginComponent {
	constructor(public router: Router, public http: Http) {}

	login(event, email, password) {
		event.preventDefault()

		let req = new XMLHttpRequest()
		req.open("POST", 'http://127.0.0.1:8181/login')
		req.onload = function() {
			switch(req.status) {
				case 200:
					self.location.href='/profile'
				break
				default:
					alert(req.response)
				break
			}
        };
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
		req.onerror = function() {
			alert(req.response)
        }
		req.send('email=' + email + '&password=' + password)
	}
}
