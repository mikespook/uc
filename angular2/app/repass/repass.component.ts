import { Component } from '@angular/core';

@Component({
  template: `
<h2>Retrieve Password</h2>
<p><a [routerLink]="['/login']">Login</a></p>
<p><a [routerLink]="['/signup']">Signup</a></p>
<form role="form" (submit)="repass($event, email.value)">
	<input #email id="email" type="text" Placeholder="Email">
	<button type="submit">Retrieve Password</button>
</form>
  `
})

// Component class
export class RepassComponent {}
