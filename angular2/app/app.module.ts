import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RepassComponent } from './repass/repass.component';
import { ProfileComponent } from './profile/profile.component';
import { SignoutComponent } from './signout/signout.component';


@NgModule({
  imports: [ BrowserModule, LoginComponent, SignupComponent, 
  RepassComponent, ProfileComponent, SignoutComponent ],
  declarations: [ AppComponent, LoginComponent, SignupComponent, 
  RepassComponent, ProfileComponent, SignoutComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
