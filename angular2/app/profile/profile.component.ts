import { Component } from '@angular/core';

@Component({
	template: `
<h2>Profile</h2>
<p><a [routerLink]="['/signout']">Signout</a></p>
<form role="form" (submit)="signup($event, password.value, repass.value, name.value, address.value, city.value, state.value, country.value, tel.value)">
	<label>{{Email}}</label>
	<input #password id="password" type="password" Placeholder="Password">
	<input #repass id="repass" type="password" Placeholder="Repeat Password">

	<input #name id="name" type="text" Placeholder="Name" value="{{Name}}">
	<input #address id="address" type="text" Placeholder="Address" value="{{Address}}">
	<input #city id="city" type="text" Placeholder="City" value="City">
	<input #state id="state" type="text" Placeholder="State" value="State">
	<input #country id="country" type="text" Placeholder="Country" value="Country">
	<input #tel id="tel" type="text" Placeholder="Tel" value="{{Tel}}">
	<button type="submit">Edit</button>
</form>`
})

// Component class
export class ProfileComponent {
	Email: string
	Name: string
	Address: string
	City: string
	State: string
	Country: string
	Tel: string
	constructor() {
		let req = new XMLHttpRequest()
		req.open("GET", 'http://127.0.0.1:8181/profile')
		req.onload = function() {
			switch(req.status) {
				case 200:
					let body = JSON.parse(req.response); 
					this.Email = body.Email
					this.Address = body.Address
					this.City = body.City
					this.State = body.State
					this.Country = body.Country
					this.Tel = body.Tel
				break
				default:
					alert(req.response)
				break
			}
        };
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
		req.onerror = function() {
			alert(req.response)
        }
		req.send()
		
	}
}
