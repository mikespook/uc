import { Component } from '@angular/core';

// Import router directives
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  selector: 'uc',
  template: '<router-outlet></router-outlet>',
  // Tell component to use router directives
  directives: [ROUTER_DIRECTIVES]
})

// App Component class
export class AppComponent {}
