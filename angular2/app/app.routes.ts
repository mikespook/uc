import { provideRouter, RouterConfig } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RepassComponent } from './repass/repass.component';
import { ProfileComponent } from './profile/profile.component';
import { SignoutComponent } from './signout/signout.component';


export const routes: RouterConfig = [
	{ path: '', redirectTo: '/login', pathMatch: 'full'},
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'repass', component: RepassComponent },

	{ path: 'profile', component: ProfileComponent },
	{ path: 'signout', component: SignoutComponent }
];

// Export routes
export const APP_ROUTER_PROVIDERS = [
	provideRouter(routes)
];
