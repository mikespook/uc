package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/mikespook/possum/session"
	"gopkg.in/mgo.v2/bson"
)

var (
	newSession = session.NewFactory(session.CookieStorage("session-id", nil))
)

/*
Common functions
*/

// output error
func writeErrorf(w http.ResponseWriter, status int, f string, a ...interface{}) {
	http.Error(w, fmt.Sprintf(f, a...), status)
}

// check method
func method(m map[string]http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		f, ok := m[r.Method]
		if !ok {
			writeErrorf(w, http.StatusMethodNotAllowed, "Respecting %v, %s got", m, r.Method)
			return
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		f(w, r)
	}
}

/*
Public handlers
*/

func loginHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	email := r.Form.Get("email")
	if email == "" {
		writeErrorf(w, http.StatusBadRequest, "Request Email")
		return
	}
	password := r.Form.Get("password")
	if password == "" {
		writeErrorf(w, http.StatusBadRequest, "Request Password")
		return
	}
	// fetch profile by email
	user := NewUser()
	if err := usersC.Find(bson.M{"email": email}).One(&user); err != nil {
		writeErrorf(w, http.StatusForbidden, "Email and Password unmatched")
		return
	}
	if user.Plain == nil {
		writeErrorf(w, http.StatusForbidden, "Email and Password unmatched")
		return
	}
	// compare password
	if !user.Auth([]byte(password)) {
		writeErrorf(w, http.StatusForbidden, "Email and Password unmatched")
		return
	}
	session, err := newSession(w, r)
	if err != nil {
		writeErrorf(w, http.StatusInternalServerError, "Session Error: %s", err)
		return
	}
	// update session
	session.Set("id", user.ID.Hex())
	session.Flush()
}

func signupHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	// insert profile
	email := r.Form.Get("email")
	if email == "" {
		writeErrorf(w, http.StatusBadRequest, "Request Email")
		return
	}
	password := r.Form.Get("password")
	if password == "" {
		writeErrorf(w, http.StatusBadRequest, "Request Password")
		return
	}
	repassword := r.Form.Get("repassword")
	if repassword != password {
		writeErrorf(w, http.StatusConflict, "Password not matched")
		return
	}
	user := NewUser()
	user.Email = email
	user.SetPassword([]byte(password))
	user.Name = r.Form.Get("name")
	user.Address = r.Form.Get("address")
	user.City = r.Form.Get("city")
	user.State = r.Form.Get("state")
	user.Country = r.Form.Get("country")
	user.Tel = r.Form.Get("tel")
	if err := usersC.Insert(user); err != nil {
		writeErrorf(w, http.StatusConflict, "User existed")
		return
	}
	session, err := newSession(w, r)
	if err != nil {
		writeErrorf(w, http.StatusInternalServerError, "Session Error: %s", err)
		return
	}
	// update session
	session.Set("id", user.ID.Hex())
	session.Flush()
}

func repassHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	// fetch profile by email
	email := r.Form.Get("email")
	if email == "" {
		writeErrorf(w, http.StatusBadRequest, "Request Email")
		return
	}
	// fetch profile by email
	user := NewUser()
	if err := usersC.Find(bson.M{"email": email}).One(&user); err != nil {
		writeErrorf(w, http.StatusNotFound, "Email not exist")
		return
	}
	if user.Plain == nil {
		writeErrorf(w, http.StatusForbidden, "Illegal auth method")
		return
	}
	// send email
	// TODO send(user.Email, user.Plain.Password)
	w.Write([]byte(user.Email))
}

/*
Private handlers
*/

func profileHandler(w http.ResponseWriter, r *http.Request) {
	session, err := newSession(w, r)
	if err != nil {
		writeErrorf(w, http.StatusInternalServerError, "Session Error: %s", err)
		return
	}
	idstr, ok := session.Get("id").(string)
	if !ok {
		writeErrorf(w, http.StatusUnauthorized, "")
		return
	}
	id := bson.ObjectIdHex(idstr)
	var user User
	if err := usersC.FindId(id).One(&user); err != nil {
		writeErrorf(w, http.StatusUnauthorized, "")
		return
	}
	if r.Method == "POST" {
		r.ParseForm()
		// update profile
		password := r.Form.Get("password")
		repassword := r.Form.Get("repassword")
		if password != "" {
			if repassword != password {
				writeErrorf(w, http.StatusConflict, "Password not matched")
				return
			}
			user.SetPassword([]byte(password))
		}
		user.Name = r.Form.Get("name")
		user.Address = r.Form.Get("address")
		user.City = r.Form.Get("city")
		user.State = r.Form.Get("state")
		user.Country = r.Form.Get("country")
		user.Tel = r.Form.Get("tel")
		if err := usersC.UpdateId(id, user); err != nil {
			writeErrorf(w, http.StatusInternalServerError, "JSON: %s", err)
		}
	}
	// output
	enc := json.NewEncoder(w)
	if err := enc.Encode(user); err != nil {
		writeErrorf(w, http.StatusInternalServerError, "JSON: %s", err)
		return
	}
	// fetch user profile
	session.Flush()
}

func signoutHandler(w http.ResponseWriter, r *http.Request) {
	session, err := newSession(w, r)
	if err != nil {
		writeErrorf(w, http.StatusUnauthorized, "Session Error: %s", err)
		return
	}
	session.Clean()
	session.Flush()
}
