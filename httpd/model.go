package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	usersC    *mgo.Collection
	dbSession *mgo.Session
)

// Basie profile
type User struct {
	ID bson.ObjectId `bson:"_id,omitempty"`

	// basic profile
	Name    string
	Address string
	City    string
	State   string
	Country string
	Email   string `bson:",omitempty"`
	Tel     string
	Date    map[string]time.Time
	// plain auth
	Plain *Plain

	// LinkedIn
	LinkedIn string
}

// Auth with password
type Plain struct {
	Password []byte
	Salt     []byte
}

// init user profile
func NewUser() (user User) {
	user.ID = bson.NewObjectId()
	user.Date = map[string]time.Time{
		"register": time.Now(),
		"update":   time.Now(),
	}
	user.Plain = &Plain{}
	return
}

// test password
func (user *User) Auth(password []byte) bool {
	// don't hash password
	// password = GenPass(user.Email, password, user.Plain.Salt)
	return bytes.Compare(password, user.Plain.Password) == 0
}

// update password
func (user *User) SetPassword(password []byte) {
	// don't hash password
	/*
		user.Plain.Salt = GenSalt(64)
		user.Plain.Password = GenPass(user.Email, password, user.Plain.Salt)
	*/
	user.Plain.Password = password
}

// generate password
func GenPass(phrase string, password, salt []byte) []byte {
	h := sha1.New()
	h.Write([]byte(phrase))
	h.Write(password)
	h.Write(salt)
	return h.Sum(nil)
}

// generate salt
func GenSalt(size int) []byte {
	b := make([]byte, size)
	if _, err := rand.Read(b); err != nil {
		return nil
	}
	return b
}

func connDB(url string) (err error) {
	dbSession, err = mgo.Dial(url)
	if err != nil {
		return
	}
	dbSession.SetMode(mgo.Monotonic, true)
	usersC = dbSession.DB("cyza").C("users")
	return nil
}

func closeDB() {
	dbSession.Close()
}
