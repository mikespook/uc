API
===

Method
------

 * `GET`: read only, no side effect
 * `POST`: read/write, side effect

HTTP Code
---------

 * 200: Success

 * 400: Bad Request - Paramaters missed or invalid
 * 401: Unauthorized - User not login
 * 404: Not found - User not found
 * 405: Method Not Allowed
 * 409: Conflict - The identification already there

 * 500: Internal Server Error

Export
------
 * "/login" POST(200, 400, 404, 405)
 * "/signup" POST(200, 400, 405, 409)
 * "/repass" POST(200, 400, 404, 405)
 * "/profile" GET/POST(200, 401, 404, 405)
 * "/signout" GET(200, 401, 405)
