package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/linkedin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	LinkedInClientID    = "78x42h1g19g97p"
	LinkedInSecret      = "EUj7aMCJptXjPos2"
	LinkedInRedirectURL = "http://cyza.520.co.nz/linkedin/callback"
)

func linkedinHandler(w http.ResponseWriter, r *http.Request) {
	urlStr := newOAuthConfig().AuthCodeURL("test")
	http.Redirect(w, r, urlStr, http.StatusFound)
}

func linkedinCallbackHandler(w http.ResponseWriter, r *http.Request) {
	token, err := newOAuthConfig().Exchange(oauth2.NoContext, r.FormValue("code"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	client := newOAuthConfig().Client(oauth2.NoContext, token)
	req, err := http.NewRequest("GET", "https://api.linkedin.com/v1/people/~:(id,email-address,formatted-name,location)?format=json", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	req.Header.Set("Bearer", token.AccessToken)
	response, err := client.Do(req)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer response.Body.Close()
	str, err := ioutil.ReadAll(response.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var inuser struct {
		ID       string `json:"id"`
		Email    string `json:"emailAddress"`
		Name     string `json:"formattedName"`
		Location struct {
			Country struct {
				Code string
			}
			Name string
		}
	}

	err = json.Unmarshal(str, &inuser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user := NewUser()
	if err := usersC.Find(bson.M{"linkedin": inuser.ID}).One(&user); err != nil && err != mgo.ErrNotFound {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	user.Email = inuser.Email
	user.Name = inuser.Name
	user.Address = inuser.Location.Name
	user.Country = inuser.Location.Country.Code
	user.LinkedIn = inuser.ID
	if _, err := usersC.UpsertId(user.ID, user); err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	session, err := newSession(w, r)
	if err != nil {
		writeErrorf(w, http.StatusInternalServerError, "Session Error: %s", err)
		return
	}
	// update session
	session.Set("id", user.ID.Hex())
	session.Flush()
	http.Redirect(w, r, "/profile.html", http.StatusFound)
}

func newOAuthConfig() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     LinkedInClientID,
		ClientSecret: LinkedInSecret,
		RedirectURL:  LinkedInRedirectURL,
		Scopes:       []string{"r_basicprofile", "r_emailaddress"},
		Endpoint:     linkedin.Endpoint,
	}
}
