/*
Cyza User Center
*/
package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/mikespook/golib/signal"
)

var (
	addr, db string
	handlers = map[string]http.HandlerFunc{
		// public
		"/login": method(map[string]http.HandlerFunc{
			"POST": loginHandler,
		}),
		"/signup": method(map[string]http.HandlerFunc{
			"POST": signupHandler,
		}),
		"/repass": method(map[string]http.HandlerFunc{
			"POST": repassHandler,
		}),
		// private
		"/profile": method(map[string]http.HandlerFunc{
			"POST": profileHandler,
			"GET":  profileHandler,
		}),
		// signout will use session internally
		"/signout": method(map[string]http.HandlerFunc{
			"GET": signoutHandler,
		}),
		"/linkedin": method(map[string]http.HandlerFunc{
			"GET": linkedinHandler,
		}),
		"/linkedin/callback": method(map[string]http.HandlerFunc{
			"GET": linkedinCallbackHandler,
		}),
	}
)

func init() {
	flag.StringVar(&addr, "addr", "127.0.0.1:8181", "Server address")
	flag.StringVar(&db, "db", "localhost", "MongoDB URL")
	flag.Parse()
}

func main() {
	signal.Bind(os.Interrupt, func() uint {
		log.Println("Interrupt signal")
		closeDB()
		return signal.BreakExit
	})

	if err := connDB(db); err != nil {
		log.Println(err)
		return
	}

	// init handlers
	for k, v := range handlers {
		http.HandleFunc(k, v)
	}

	go func() {
		log.Printf("Listen on %s\n", addr)
		if err := http.ListenAndServe(addr, nil); err != nil {
			log.Println(err)
			if err := signal.Send(os.Getpid(), os.Interrupt); err != nil {
				panic(err)
			}
			return
		}
	}()
	signal.Wait()
}
